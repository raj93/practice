
n=input()

def fib():
	a, b = 0, 1
	for i in range(n):
		yield a
		a, b = b, a + b
	
b = fib()
b.next()


for i in fib():
	print(i)

