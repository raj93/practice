#!flask/bin/python
from app import app
from flask_wtf.csrf import CsrfProtect

csrf = CsrfProtect()
csrf.init_app(app)

app.run(debug=True)
