__author__ = 'rlora'

from string import *
import re

def constrainedMatchPair(firstMatch, secondMatch, length):
    a = ();
    for i in firstMatch:
        for j in secondMatch:
            print i
            if j == (i + length + 1):
                a = a + (j,)
                #print("This is a", a)
        return a

def subStringMatchExact(key, target):
    c = re.finditer(key, target)
    l = ();
    for i in c:
        l += (i.start(),)
    return l


def subStringMatchOneSub(key,target):
    """search for all locations of key in target, with one substitution"""
    allAnswers = ()
    for miss in range(0,len(key)):
        # miss picks location for missing element
        # key1 and key2 are substrings to match
        key1 = key[:miss]
        key2 = key[miss+1:]
        #print("key1", key1)
        print 'breaking key',key,'into',key1,key2
        # match1 and match2 are tuples of locations of start of matches
        # for each substring in target
        match1 = subStringMatchExact(key1, target)
        #print match1
        match2 = subStringMatchExact(key2, target)
        # when we get here, we have two tuples of start points
        # need to filter pairs to decide which are correct
        filtered = constrainedMatchPair(match1,match2,len(key1))
        #print filtered
        allAnswers = allAnswers + filtered
        print 'match1',match1
        print 'match2',match2
        print 'possible matches for', key1, key2,'start at', filtered
    return allAnswers



subStringMatchOneSub("atg", "atgacatgcacaagtatgcat")
#subStringMatchExact("atgc", "atgacatgcacaagtatgcatatgc")