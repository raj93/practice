#!/usr/bin/python

class CashRegister:

    def __init__(self,loonies, two, fives, tens, twenties):

        self.loonies = loonies
        self.two = two
        self.fives = fives
        self.tens = tens
        self.twenties = twenties

    def get_total(self):
            return self.loonies + self.two * 2 + self.fives * 5 + self.tens * 10 + self.twenties * 20

    def add(self, count, denomination):
            if denomination == 'loonies':
                self.loonies += count
            elif denomination == 'two':
                self.two += count
            elif denomination == 'fives':
                self.tens += count
            elif denomination == 'tens':
                self.tens += count
            elif denomination == 'wenties':
                self.twenties += count

    def remove(self, count, denomination):
            if denomination == 'loonies':
                self.loonies -= count
            elif denomination == 'two':
                self.two -= count
            elif denomination == 'fives':
                self.tens -= count
            elif denomination == 'tens':
                self.tens -= count
            elif denomination == R'twenties':
                self.twenties -= count

if __name__ == '__main__':
    register = CashRegister(5,5,5,5,5)
    print(register.get_total())

    register.add(3, 'two')
    register.remove(2, 'twenties')

    print(register.get_total())

