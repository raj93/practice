from string import *


def countSubStringMatch(target,key):
    return find(target, key)

def countSubStringMatchRecursive(target,key, afterposition):
    return find(target, key, afterposition)


print countSubStringMatch("atgacatgcacaagtatgcat","atgc")
print countSubStringMatchRecursive("atgacatgcacaagtatgcat","atgc",6)

