

def foo(one, two, *three):
	count = 0
	for i in three:
		count += 1
	print count


def bar(first, second, **three):
	if three.get("seven") == 7:
		print True
	else:
		print False

result = foo(1, 2, 3, 4, 5)
result1 = bar(1, 2, three = 3, seven = 7)
print result
print result1
