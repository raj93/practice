def bsearch(l, n):

    a=0;
    b=len(l)-1
    result=0
    while b>=a:
        m = (a+b)//2
        if m>n:
            a=m+1
        else:
            a=b-1

    if a == len(l) or l[a] != n:
        return -1
    else:
        return 1

l = [1,2,3,4,5]
print(bsearch(l,2))
